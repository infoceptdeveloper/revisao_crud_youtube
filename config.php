<?php
require 'environment.php';

global $config;
global $db;

$config = array();
if (ENVIRONMENT == 'development') {
	define("BASE_URL", "http://localhost/revisao_crud_mvc/");
	$config['dbname'] = 'revisao_crud_mvc';
	$config['host'] = 'localhost'; //em geral o servidor é localhost
	$config['dbuser'] = 'root'; //em geral o usuário é root
	$config['dbpass'] = '';
} else {
	define("BASE_URL", "https://url_online/");
	$config['dbname'] = 'bd_online';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'user_online';
	$config['dbpass'] = 'pass_online';
}

/*Informações de e-mail*/
$config['hostmail'] = 'servidor_email';
$config['Usermail'] = 'email';
$config['Username'] = 'username';
$config['Password'] = 'password_email';

/*Informações de transporte */
$config['zipcode_origin'] = '36570000';

/**Informações de gateway de pagamento */
$config['api_key'] = 'hash_api';

/*Informações de api redes sociais */
define("FACEBOOK", [
	"app_id" => "api_id",
	"app_secret" => "app_secret",
	"app_redirect" => "http://localhost:90/vemcaver/home/login",
	"app_version" => "v17.0"
]);

define("GOOGLE", [
	"client_id" => "cliend_id",
	"client_secret" => "client_secret",
	"redirect_uris" => "http://localhost:90/vemcaver/home/login",
]);

try {
	$db = new PDO("mysql:charset=utf8;host=" . $config['host'] . ";dbname=" . $config['dbname'], $config['dbuser'], $config['dbpass']);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	echo "Entre em contato com o suporte";
	error_log("Error " . $e->getMessage(), 3, "error_bd.log");
}
