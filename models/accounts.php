<?php

class accounts extends Model
{
    public function add($name, $login, $password)
    {
        $sql = "INSERT INTO accounts (name, login, password, responsibility) VALUES (:name, :login, :password)";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":name", $name);
        $sql->bindValue(":login", $login);
        $sql->bindValue(":password", $password);
        $sql->execute();

        if ($sql->rowCount() > 0) {
            $id = $this->db->lastInsertId(); //pega a última ID inserida e vamos armazenar em outra tabela como chave estrangeira
            $_SESSION['name_project_online'] = $id;
            return true;
        } else {
            return false;
        }
    }

    public function checkLogin($login, $password)
    {

        $sql = "SELECT password, id FROM accounts WHERE email = :login OR phone = :login OR nickname = :login";
        $sql = $this->db->prepare($sql);
        $sql->bindValue(":login", $login);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            $data = $sql->fetch(PDO::FETCH_ASSOC);
            if (password_verify($password, $data['password'])) {
                $_SESSION['name_project_online'] = $data['id'];
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
