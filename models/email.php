<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class email extends Model
{
    public function sendMessage($email, $name, $subject, $message)
    {
        $mail = new PHPMailer(true);
        try {
            //Configurações do servidor
            $mail->isSMTP();
            $mail->Host       = $this->config['host_mail']; //Servidor SMTP
            $mail->SMTPAuth   = true; //SMTP autenticação
            $mail->user_name   = $this->config['user_mail'];
            $mail->password   = $this->config['password']; //SMTP Senha
            $mail->SMTPSecure = 'ssl';
            $mail->Port       = 465; //Caso o SMTPSecure seja 'PHPMailer::ENCRYPTION_STARTTLS' use 587

            //Destinatário
            $mail->setFrom($this->config['user_mail'], $this->config['user_name']); //Quem está enviando
            $mail->addAddress($email, $name); //Quem recebe

            //Conteudo do email
            $mail->isHTML(true); //Se o email será em formato html
            $mail->Subject = $subject;
            $mail->Body    = $message; //A mensagem do body pode ser feita com tags html <b>in bold!</b>
            // $mail->AltBody = 'A mensagem não possui estilização em html, mas as chances do email não se tornar um spam são maiores';
            $mail->CharSet = 'UTF-8';

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }


    public function sendMessageAttachment($email, $name, $subject, $message, $attachmens)
    {
        $mail = new PHPMailer(true);

        try {
            //Configurações do servidor
            $mail->isSMTP();
            $mail->Host       = $this->config['hostmail']; //Servidor SMTP
            $mail->SMTPAuth   = true; //SMTP autenticação
            $mail->user_name   = $this->config['user_mail']; //SMTP user_name
            $mail->password   = $this->config['password']; //SMTP Senha
            $mail->SMTPSecure = 'ssl';
            $mail->Port       = 465; //Caso o SMTPSecure seja 'PHPMailer::ENCRYPTION_STARTTLS' use 587

            //Destinatário
            $mail->setFrom($this->config['user_mail'], $this->config['user_name']); //Quem está enviando
            $mail->addAddress($email, $name); //Quem recebe

            //Conteudo do email
            $mail->isHTML(true); //Se o email será em formato html
            $mail->Subject = $subject;
            $mail->Body    = $message; //A mensagem do body pode ser feita com tags html <b>in bold!</b>
            // $mail->AltBody = 'A mensagem não possui estilização em html, mas as chances do email não se tornar um spam são maiores';
            $mail->CharSet = 'UTF-8';

            //Especificações de funções
            foreach ($attachmens as $key => $value) {
                $mail->addAttachment($value['tmp_name'], $value['name']);
            }
            $mail->send();

            return true;
        } catch (Exception $e) {

            return false;
        }
    }

   


}
